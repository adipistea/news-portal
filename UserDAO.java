package infrastructure.data.dao;

import domain.entities.User;
import infrastructure.data.interfaces.IUserDAO;
import common.PasswordHasher;


public class UserDAO extends GenericDAO<User> implements IUserDAO {
    public UserDAO() {
        super(User.class);
    }

    @Override
    public User create(User entity) {

        try {
            String hashedPassword = PasswordHasher.getSaltedHash(entity.getPassword());
            entity.setPassword(hashedPassword);
            User userInserted = super.create(entity);
            return userInserted;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public User findByUserName(String userName){
        try{
            for(User user : get()){
                if(userName.equals(user.getUsername())){
                    return user;
                }
            }
            return null;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
