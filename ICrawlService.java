package services.interfaces;

import javafx.model.CrawledNews;

import java.util.List;


public interface ICrawlService {
    List<CrawledNews> crawl(String url);

    List<CrawledNews> crawlRssUrl(String url);
}
