package domain.entities;



public enum FriendRequest {
    Unanswered,
    Accepted,
    Declined,
}
