package javafx.controller.friend;

import javafx.AppNavigation;
import javafx.FXMLResources;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.News;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import services.NewsService;

import java.net.URL;
import java.util.ResourceBundle;


public class FriendNewsController implements Initializable {
    @FXML
    private TableView<News> friendNewsTable;
    @FXML
    private TableColumn<News, String> titleColumn;

    private ObservableList<News> newsList;
    private NewsService newsService;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newsService = new NewsService();
        newsList = FXCollections.observableArrayList();

        // Initialize the person table with the two columns.
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
        newsList.clear();
        newsList.addAll(newsService.get(Keeper.getFriendship().getFriend().getId()));

        friendNewsTable.setItems(newsList);
    }

    public void addCommentAction(ActionEvent actionEvent) {
        News news = friendNewsTable.getSelectionModel().getSelectedItem();

        if(news!=null) {
            Keeper.setNews(news);
            AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.addCommentForFriend));
        }
    }
}
