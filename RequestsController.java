package javafx.controller.friend;

import common.CurrentUser;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Friendship;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import services.FriendshipService;

import java.net.URL;
import java.util.ResourceBundle;


public class RequestsController implements Initializable{

    @FXML
    private TableView<Friendship> requestsTable;
    @FXML
    private TableColumn<Friendship, String> nameColumn;


    private FriendshipService friendshipService;
    private ObservableList<Friendship> friendships;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getFriend().fullNameProperty());
        friendships = FXCollections.observableArrayList();

        friendshipService = new FriendshipService();
        friendships.clear();
        friendships.addAll(friendshipService.getAllFriendRequests(CurrentUser.getLoggedInUser()));

        requestsTable.setItems(friendships);
    }

    public void acceptAction(ActionEvent event){

        Friendship friendship = requestsTable.getSelectionModel().getSelectedItem();

        if(friendship!=null){
            friendshipService.acceptFriendRequest(friendship);

            friendships.remove(friendship);
        }
    }

    public void declineAction(ActionEvent event){
        Friendship friendship = requestsTable.getSelectionModel().getSelectedItem();

        if(friendship!=null){
            friendshipService.declineFriendRequest(friendship);

            friendships.remove(friendship);
        }
    }
}
