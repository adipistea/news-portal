package common.mapping;

import javafx.model.Site;

import java.util.List;


public class SiteToSiteDb implements GenericMapper<Site, domain.entities.Site> {
    @Override
    public domain.entities.Site map(Site site) {


        return site!=null ?  new domain.entities.Site(site.getUrl(), site.getRssUrl()) : null;
    }
}
