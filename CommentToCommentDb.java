package common.mapping;

import javafx.model.Comment;

/**
 * Created by Feri on 6/3/2015.
 */
public class CommentToCommentDb implements GenericMapper<Comment, domain.entities.Comment> {
    private UserToUserDb userToUserDb;
    private NewsToNewsDb newsToNewsDb;

    public CommentToCommentDb() {
        this.userToUserDb = new UserToUserDb();
        this.newsToNewsDb = new NewsToNewsDb();
    }

    @Override
    public domain.entities.Comment map(Comment comment) {


        return comment!=null ? new domain.entities.Comment(comment.getContent(), comment.getDateAdded(),
                newsToNewsDb.map(comment.getNews()),
                userToUserDb.map(comment.getFriend())) : null;
    }
}
