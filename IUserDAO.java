package infrastructure.data.interfaces;

import domain.entities.User;


public interface IUserDAO extends IGenericDAO<User> {

    User findByUserName(String userName);
}
