package javafx.controller.friend;

import common.CurrentUser;
import javafx.AppNavigation;
import javafx.FXMLResources;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Friendship;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import jfx.messagebox.MessageBox;
import services.FriendshipService;

import java.net.URL;
import java.util.ResourceBundle;


public class DisplayAllFriendsController implements Initializable {

    @FXML
    private TableView<Friendship> friendsTable;
    @FXML
    private TableColumn<Friendship, String> friendsColumn;

    @FXML
    private TextField friendNameField;

    private FriendshipService friendshipService;
    private ObservableList<Friendship> friendships;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        // Initialize the person table with the two columns.
        friendsColumn.setCellValueFactory(cellData -> cellData.getValue().getFriend().userNameProperty());
        friendships = FXCollections.observableArrayList();
        friendshipService = new FriendshipService();

        friendships.clear();
        friendships.addAll(friendshipService.get());
        friendsTable.setItems(friendships);
    }

    public void addFriendAction(ActionEvent event) {
        if (!friendNameField.getText().equals("")) {
            friendshipService.sendFriendRequest(CurrentUser.getLoggedInUser(), friendNameField.getText());
            //TODO:: display not found window
            MessageBox.show(AppNavigation.getStage(),
                    "Added successfully",
                    "Success",
                    MessageBox.ICON_INFORMATION);
        }
    }

    public void deleteFriendAction(ActionEvent event) {
        Friendship friendship = friendsTable.getSelectionModel().getSelectedItem();
        if (friendship != null) {
            friendshipService.delete(friendship.getId());
            friendships.remove(friendship);
            MessageBox.show(AppNavigation.getStage(),
                    "Removed successfully",
                    "Success",
                    MessageBox.ICON_INFORMATION);
        }
    }

    public void changeRightsAction(ActionEvent event) {
        Friendship friendship = friendsTable.getSelectionModel().getSelectedItem();
        if (friendship != null) {
            Keeper.setFriendship(friendship);
        }
        AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.giveRights));
    }

    public void friendNewsAction(ActionEvent event) {
        Friendship friendship = friendsTable.getSelectionModel().getSelectedItem();
        //TODO:: add navigation
        if (friendship != null) {
            Keeper.setFriendship(friendship);
            AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.friendNews));
        }
    }
}
