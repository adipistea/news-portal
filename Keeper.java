package javafx.controller.friend;

import javafx.model.Friendship;
import javafx.model.News;


public class Keeper {

    private static Friendship friend;

    public static void setFriendship(Friendship friend) {
        Keeper.friend = friend;
    }

    public static Friendship getFriendship() {
        return friend;
    }

    private static News news;

    public static News getNews() {
        return news;
    }

    public static void setNews(News news) {
        Keeper.news = news;
    }
}
