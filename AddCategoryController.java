package javafx.controller.category;

import javafx.AppNavigation;
import javafx.FXMLResources;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Category;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import services.CategoryService;

import java.net.URL;
import java.util.ResourceBundle;


public class AddCategoryController implements Initializable {


    @FXML
    private TextField nameField;

    @FXML
    private TextField descriptionField;

    private ObservableList<Category> categoryList;
    private CategoryService categoryService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        categoryService = new CategoryService();
    }

    public void addAction(ActionEvent event) {
        String name = nameField.getText();
        String description = descriptionField.getText();
        categoryService.add(new Category(null, name, description));
        AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.displayCategories));
    }
}
