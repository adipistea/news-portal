package infrastructure.data.dao;


import common.DatabaseException;
import domain.entities.News;
import infrastructure.JpaUtil;
import infrastructure.data.interfaces.INewsDAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class NewsDAO extends GenericDAO<News> implements INewsDAO {
    public NewsDAO() {
        super(News.class);
    }

    public List<News> getForUser(Long id) throws DatabaseException {
        String sql = new String("select c from " + News.class.getName() + " c  where c.user.id =:arg1");
        EntityManager entityManager = JpaUtil.getEntityManager();
        Query query = entityManager.createQuery(sql);
        query.setParameter("arg1", id);

        List<News> news = new ArrayList<>();
        try {
            news.addAll(query.getResultList());
        } catch (Exception e) {
            throw new DatabaseException(e.getMessage());
        } finally {
            entityManager.close();
        }

        return news;
    }
}
