package common.mapping;

import domain.entities.Site;


public class SiteDbToSite implements GenericMapper<Site, javafx.model.Site> {
    @Override
    public javafx.model.Site map(Site site) {

        return site!=null ? new javafx.model.Site(site.getId(), site.getUrl(), site.getRssFeedUrl()) : null;
    }
}
