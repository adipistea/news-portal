package services;

import common.CurrentUser;
import common.DatabaseException;
import common.mapping.Mapper;
import domain.entities.Category;
import domain.entities.Site;
import infrastructure.data.dao.UnitOfWork;
import javafx.AppNavigation;
import javafx.model.CrawledNews;
import javafx.model.News;
import jfx.messagebox.MessageBox;

import java.util.ArrayList;
import java.util.List;


public class NewsService {

    private UnitOfWork unitOfWork;
    private Mapper mapper;

    public NewsService() {
        unitOfWork = new UnitOfWork();
        mapper = new Mapper();
    }


    public List<News> get() {
        List<domain.entities.News> newsDb = null;
        try {
            newsDb = unitOfWork.getNewsDAO().get();
        } catch (DatabaseException e) {
            MessageBox.show(AppNavigation.getStage(),
                    "Database error",
                    "Error",
                    MessageBox.ICON_ERROR);
        }
        return mapper.getNewsDbToNews().mapList(newsDb);
    }


    public List<News> get(Long id) {
        List<domain.entities.News> newsDb = null;
        try {
            newsDb = unitOfWork.getNewsDAO().getForUser(id);
        } catch (DatabaseException e) {
            MessageBox.show(AppNavigation.getStage(),
                    "Database error",
                    "Error",
                    MessageBox.ICON_ERROR);
        }
        return mapper.getNewsDbToNews().mapList(newsDb);
    }


    public void delete(Long id) {
        unitOfWork.getNewsDAO().remove(id);
    }

    public void add(CrawledNews crawledNews) {


        domain.entities.News news = new domain.entities.News(crawledNews.getTitle(), crawledNews.getTitle(), crawledNews.getPubDate(),
                crawledNews.getLink(),null, null);

        news.setUser(CurrentUser.getLoggedInUser());
        unitOfWork.getNewsDAO().create(news);
    }

    public void updateCategory(News news, Long categoryId){
        domain.entities.News newsDb = unitOfWork.getNewsDAO().find(news.getId());

        Category categoryDb = unitOfWork.getCategoryDAO().find(categoryId);
        newsDb.setCategory(categoryDb);

        unitOfWork.getNewsDAO().update(newsDb);
    }
}
