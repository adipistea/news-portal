package infrastructure.data.interfaces;

import domain.entities.Right;


public interface IRightDAO extends IGenericDAO<Right> {
    domain.entities.Right getByName(String name);
}
