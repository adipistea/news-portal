package javafx.controller.friend;

import common.CurrentUser;
import common.mapping.Mapper;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Comment;
import javafx.model.Friendship;
import javafx.scene.control.TextField;
import services.CommentService;
import services.FriendshipService;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;


public class AddCommentsForFriendController implements Initializable {
    @FXML
    public TextField contentField;

    private CommentService commentService;
    private Mapper mapper;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        commentService = new CommentService();
        mapper = new Mapper();
    }

    public void addAction(ActionEvent actionEvent) {
        String text = contentField.getText();
        if (text != "") {
            Date dateUtil = new Date();
            java.sql.Date date = new java.sql.Date(dateUtil.getTime());

            Comment comment = new Comment(null, text, date, Keeper.getNews(),
                    mapper.getUserDbToUser().map(CurrentUser.getLoggedInUser()));
            commentService.add(comment);
        }
    }
}