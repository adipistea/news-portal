package services;

import common.CurrentUser;
import common.DatabaseException;
import domain.entities.User;
import infrastructure.data.dao.UnitOfWork;
import common.PasswordHasher;
import javafx.AppNavigation;
import jfx.messagebox.MessageBox;
import services.interfaces.IAuthenticateService;

import java.util.List;


public class AuthenticateService implements IAuthenticateService {

    private UnitOfWork unitOfWork;

    public AuthenticateService() {
        this.unitOfWork = new UnitOfWork();
    }

    public boolean authenticate(String userName, String password) {

        List<User> users = null;
        try {
            users = unitOfWork.getUserDAO().get();
        } catch (DatabaseException e) {

            MessageBox.show(AppNavigation.getStage(),
                    "Database error",
                    "Error",
                    MessageBox.ICON_ERROR);
        }

        for (User user : users){
            if(user.getUsername().equals(userName)){
                try {
                    return PasswordHasher.check(password, user.getPassword());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public void preserveLoggedUser(String userName){

        User loggedInUser = unitOfWork.getUserDAO().findByUserName(userName);

        if(loggedInUser!=null){
            CurrentUser.setLoggedInUser(loggedInUser);
        }
    }
}
