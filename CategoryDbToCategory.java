package common.mapping;

import domain.entities.Category;


public class CategoryDbToCategory implements GenericMapper<Category, javafx.model.Category> {
    @Override
    public javafx.model.Category map(Category category) {
        return category!=null ? new javafx.model.Category(category.getId(), category.getName(), category.getDescription())
        : null;
    }
}
