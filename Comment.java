package domain.entities;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Date;

@Entity
@Table(name = "Comments3" +
        "")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private Long id;

    @Column(name="Content")
    private String content;

    @Column(name="DateAdded")
    private Date dateAdded;

    @JoinColumn(name = "NewsId", referencedColumnName = "Id")
    @ManyToOne
    private News news;

    @JoinColumn(name="UserId", referencedColumnName = "Id")
    @ManyToOne
    private User friend;

    public Comment(){}

    public Comment(Long id){this.id = id;}

    public Comment(String content, Date dateAdded, News news, User friend) {
        this.content = content;
        this.dateAdded = dateAdded;
        this.news = news;
        this.friend = friend;
    }

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public News getNews() {
        return news;
    }

    public User getFriend() {
        return friend;
    }
}
