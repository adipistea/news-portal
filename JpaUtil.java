package infrastructure;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {

    private static EntityManagerFactory emf = null;

    public JpaUtil() {
    }

    /**
     * Create domain.entities manager factory.
     *
     * @return domain.entities manager factory from persistence unit
     */
    public static EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("unitHibernate", new java.util.HashMap());
        }
        return emf;
    }

    /**
     * Close the domain.entities manager factory.
     */
    public static void closeEntityManagerFactory() {
        if (emf != null) {
            emf.close();
        }
    }

    /**
     * Gets new domain.entities manager instance.
     *
     * @return new domain.entities manager instance
     */
    public static EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }
}