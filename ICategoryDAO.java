package infrastructure.data.interfaces;

import domain.entities.Category;


public interface ICategoryDAO extends IGenericDAO<Category> {
}
