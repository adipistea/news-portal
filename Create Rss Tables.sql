Create Table if not exists Users(
Id BIGINT NOT NULL auto_increment,
FirstName varchar(256),
LastName varchar(256),
UserName varchar(256),
Password varchar(256),
Primary key(Id)
);

Create Table if not exists News2(
Id BigINT NOT null AUTO_INCREMENT,
Content varchar(256),
Title varchar(256),
DataAdded datetime,
Link varchar(256),
SiteID BIGINT ,
UserID BIGINT, 
Primary Key(Id),
Foreign Key(SiteID) References Sites(Id),
Foreign Key(UserID) References Users(Id)
);

Create Table if not exists Sites(
Id BigINT NOT null AUTO_INCREMENT,
URL varchar(256),
RSSFeedURL varchar(256) ,
Primary Key(Id)
);

Create Table if not exists Categories(
Id BigINT NOT null AUTO_INCREMENT,
Name varchar(256),
Description varchar(1000) ,
Primary Key(Id)
);

Create Table if not exists Keywords(
Id BigINT NOT null AUTO_INCREMENT,
Name varchar(256),
Primary Key(Id)
);


Create Table if not exists CategoryKeyWords(
CategoryId BigINT NOT null,
KeywordId BigINT NOT NULL,
Foreign Key(CategoryId) References Categories(Id),
Foreign Key(KeywordId) References Keywords(Id),
Primary Key(CategoryId, KeywordId)
);

Create Table if not exists Rights(
Id BigINT NOT null AUTO_INCREMENT,
Name varchar(256),
Primary Key(Id)
);

Create Table if not exists Comments2(
Id BigINT NOT null AUTO_INCREMENT,
Content varchar(1000),
DateAdded date, 
NewsId BIGINT NOT NULL,
UserId BIGINT NOT NULL,
Foreign Key(NewsId) References News2(Id),
Foreign Key(UserId) References Users(Id),
Primary Key(Id)
);


Create Table if not exists Friendships2(
  Id BigINT NOT null AUTO_INCREMENT,
  UserId BIGINT NOT NULL,
  FriendId BIGINT NOT NULL,
  AcceptedFriend int NOT NULL,
  Foreign Key(UserId) References Users(Id),
  Foreign Key(FriendId) References Users(Id),
  Primary Key(Id)
);

Create Table if not exists ContentPaths(
  Id BigINT NOT null AUTO_INCREMENT,
  SiteID BIGINT NOT NULL,
  Name varchar(1000),
  Path varchar(1000),
  Primary Key(Id),
  Foreign Key(SiteID) References Sites(Id)
);
Create Table if not exists FriendshipRights(
 FriendshipId BigINT NOT null,
 RightId BigINT NOT null,
 Foreign key(FriendshipId) references Friendships(Id),
 Foreign Key(RightId) References Rights(Id),
 Primary Key(FriendshipId, RightId)  
);