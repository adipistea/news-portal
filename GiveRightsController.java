package javafx.controller.friend;

import common.CurrentUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Right;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import services.FriendshipService;
import services.RightService;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


public class GiveRightsController implements Initializable {

    @FXML
    private CheckBox deleteCommentBox;

    @FXML
    private CheckBox addCommentBox;

    @FXML
    private CheckBox viewNewsBox;

    @FXML
    private TextField friendName;

    private FriendshipService friendshipService;
    private RightService rightService;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        friendName.setText(
                CurrentUser.getLoggedInUser().getFirstName() + " " + CurrentUser.getLoggedInUser().getLastName());
        friendshipService = new FriendshipService();
        rightService = new RightService();
    }

    public void applyAction(ActionEvent event){

        List<Right> rightsSelected = new ArrayList<>();

        if(viewNewsBox.isSelected()){
            rightsSelected.add(rightService.getByName("read"));        }

        if(addCommentBox.isSelected()){
            rightsSelected.add(rightService.getByName("add"));
        }
        if(deleteCommentBox.isSelected()){
            rightsSelected.add(rightService.getByName("delete"));
        }
        friendshipService.create(CurrentUser.getLoggedInUser(), Keeper.getFriendship().getFriend(), rightsSelected);
    }
}
