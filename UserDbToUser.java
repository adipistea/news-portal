package common.mapping;

import domain.entities.User;

import java.util.List;


public class UserDbToUser implements GenericMapper<User, javafx.model.User> {
    @Override
    public javafx.model.User map(User user) {

        return user!=null ? new javafx.model.User(user.getId(), user.getFirstName(), user.getLastName(), user.getUsername()) : null;
    }

    @Override
    public List<javafx.model.User> mapList(List<User> listSource) {
        return  new GenericListMapper(this).mapList(listSource);
    }
}
