package javafx.controller.news;

import common.Browser;
import javafx.AppNavigation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.CrawledNews;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import jfx.messagebox.MessageBox;
import services.CrawlService;
import services.NewsService;
import services.interfaces.ICrawlService;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;


public class AddNewsController implements Initializable {


    private NewsService newsService;
    private ObservableList<CrawledNews> crawledNews = FXCollections.observableArrayList();


    @FXML
    private TextField urlAdress;

    @FXML
    private TableView<CrawledNews> newsTable;
    @FXML
    private TableColumn<CrawledNews, String> titleColumn;
    public TableColumn<CrawledNews, String> linkColumn;
    public TableColumn<CrawledNews, Date> dateColumn;


    public void loadUrl(ActionEvent event) {
        ICrawlService crawlService = new CrawlService();
        String url = urlAdress.getText();
        //url = "http://www.bbc.com/news/technology";

        if (!Objects.equals(url, "")) {

            crawledNews.clear();
            //crawledNews.addAll(crawlService.crawl(url));
            crawledNews.addAll(crawlService.crawlRssUrl(url));
        } else {

            //TODO:: display message URL not found
            MessageBox.show(AppNavigation.getStage(),
                    "URL not foun",
                    "Warning",
                    MessageBox.ICON_WARNING);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newsService = new NewsService();
        // Initialize the person table with the two columns.
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
        linkColumn.setCellValueFactory(cellData -> cellData.getValue().linkProperty());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().pubDateProperty());
        newsTable.setItems(crawledNews);
    }

    public void addAction(ActionEvent event) {

        CrawledNews c = newsTable.getSelectionModel().getSelectedItem();
        if (c != null) {
            newsService.add(c);

            MessageBox.show(AppNavigation.getStage(),
                    "Added successfully",
                    "Success",
                    MessageBox.ICON_INFORMATION);
        }
    }

    public void navigateUrl(ActionEvent actionEvent) {
        CrawledNews c = newsTable.getSelectionModel().getSelectedItem();
        if (c != null) {
           Browser browser =  new Browser();
            browser.start(new Stage());
            browser.navigate(c.getLink());
        }
    }
}
