package infrastructure.data.interfaces;


public interface IUnitOfWork {

    IUserDAO getUserDAO();

    INewsDAO getNewsDAO();

    ISiteDAO getSiteDAO();

    ICategoryDAO getCategoryDAO();

    IFriendshipDAO getFriendshipDAO();

    IRightDAO getRightDAO();

    ICommentDAO getCommentDAO();
}
