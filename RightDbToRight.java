package common.mapping;

import javafx.model.Right;

import java.util.List;


public class RightDbToRight implements GenericMapper<domain.entities.Right, Right> {


    @Override
    public Right map(domain.entities.Right source) {


        return source!=null ? new Right(source.getId(), source.getName()): null;
    }

    @Override
    public List<Right> mapList(List<domain.entities.Right> listSource){
        return new GenericListMapper<domain.entities.Right, Right>(this).mapList(listSource);
    }
}
