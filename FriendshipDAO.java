package infrastructure.data.dao;

import domain.entities.FriendRequest;
import domain.entities.Friendship;
import domain.entities.Site;
import infrastructure.JpaUtil;
import infrastructure.data.interfaces.IFriendshipDAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class FriendshipDAO extends GenericDAO<Friendship> implements IFriendshipDAO{
    public FriendshipDAO() {
        super(Friendship.class);
    }


    @Override
    public List<Friendship> get(){
        String sql = new String("select c from " + Friendship.class.getName() + " c  where acceptedFriend =:arg1");
        EntityManager entityManager = JpaUtil.getEntityManager();
        Query query = entityManager.createQuery(sql);
        query.setParameter("arg1", FriendRequest.Accepted);

        List<Friendship> friendhips = new ArrayList<>();
        try {
            friendhips.addAll(query.getResultList());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }

        return friendhips;
    }


    public List<Friendship> getFriendhipsForUser(Long id){
        String sql = new String("select c from " + Friendship.class.getName() + " c  where c.acceptedFriend =:arg1 AND c.friend.id =:arg2");
        EntityManager entityManager = JpaUtil.getEntityManager();
        Query query = entityManager.createQuery(sql);
        query.setParameter("arg1", FriendRequest.Unanswered);
        query.setParameter("arg2", id);

        List<Friendship> friendhips = new ArrayList<>();
        try {
            friendhips.addAll(query.getResultList());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }

        return friendhips;
    }

    public List<Friendship> getFriendhipsForUser2(Long id){
        String sql = new String("select c from " + Friendship.class.getName() + " c  where c.acceptedFriend =:arg1 " +
                "AND c.user.id =:arg2 or c.friend.id =:arg2");
        EntityManager entityManager = JpaUtil.getEntityManager();
        Query query = entityManager.createQuery(sql);
        query.setParameter("arg1", FriendRequest.Accepted);
        query.setParameter("arg2", id);

        List<Friendship> friendhips = new ArrayList<>();
        try {
            friendhips.addAll(query.getResultList());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }

        return friendhips;
    }

}
