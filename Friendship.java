package domain.entities;



import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "Friendships2")
public class Friendship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @JoinColumn(name = "UserId", referencedColumnName = "Id")
    @ManyToOne
    private User user;

    @JoinColumn(name = "FriendId", referencedColumnName = "Id")
    @ManyToOne
    private User friend;


    @ManyToMany()
    @JoinTable(name = "FriendshipRights",
            joinColumns = @JoinColumn(name = "FriendshipId", referencedColumnName = "Id"),
            inverseJoinColumns = @JoinColumn(name = "RightId", referencedColumnName = "Id")
    )
    private List<Right> rights;

    @Column(name = "AcceptedFriend")

    private FriendRequest acceptedFriend;

    public Friendship() {
    }

    public Friendship(Long id) {
        this.id = id;
    }

    public Friendship(User user, User friend,  FriendRequest acceptedFriend, List<Right> rights) {
        this.user = user;
        this.friend = friend;
        this.rights = rights;
        this.acceptedFriend = acceptedFriend;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public User getFriend() {
        return friend;
    }

    public List<Right> getRights() {
        return rights;
    }

    public void setAcceptedFriend(FriendRequest acceptedFriend) {
        this.acceptedFriend = acceptedFriend;
    }
}

