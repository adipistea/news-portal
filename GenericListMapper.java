package common.mapping;

import common.mapping.GenericMapper;

import java.util.ArrayList;
import java.util.List;


public class GenericListMapper<TSource, TDestination> {

    private GenericMapper<TSource, TDestination> mapper;
    public GenericListMapper(GenericMapper<TSource, TDestination> mapper) {
        this.mapper = mapper;
    }

    public List<TDestination> mapList(List<TSource> listDb){
        List<TDestination> list = new ArrayList<>();
        for(TSource entity  : listDb){
            list.add(mapper.map(entity));
        }
        return  list;
    }
}
