package common.mapping;

import javafx.model.Category;


public class CategoryToCategoryDb implements GenericMapper<Category, domain.entities.Category> {
    @Override
    public domain.entities.Category map(Category category) {
        return category!=null ? new domain.entities.Category(category.getName(), category.getDescription(), null) : null;
    }
}
