package infrastructure.data.dao;

import common.DatabaseException;
import domain.entities.Right;
import infrastructure.data.interfaces.IRightDAO;
import javafx.AppNavigation;
import jfx.messagebox.MessageBox;


public class RightDAO extends GenericDAO<Right> implements IRightDAO{
    public RightDAO() {
        super(Right.class);
    }

    public domain.entities.Right getByName(String name){

        try {
            for(Right right : get()){
                if(right.getName().equals(name)){
                    return right;
                }
            }
        } catch (DatabaseException e) {
            MessageBox.show(AppNavigation.getStage(),
                    "Database error",
                    "Error",
                    MessageBox.ICON_ERROR);
        }
        return null;
    }
}
