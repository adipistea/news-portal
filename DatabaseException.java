package common;

/**
 * Created by Feri on 6/4/2015.
 */
public class DatabaseException extends Exception {

    public DatabaseException() {
    }

    public DatabaseException(String message) {
        super(message);
    }
}
