package crawler;


import java.sql.Date;


public class CrawledNews {

    private String title;

    private String description;

    private String link;

    private Date pubDate;

    public CrawledNews(String title, String description, String link, java.sql.Date date) {
        this.title = title;
        this.description = description;
        this.link = link;
        this.pubDate = date;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public Date getPubDate() {
        return pubDate;
    }
}
