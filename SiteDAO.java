package infrastructure.data.dao;

import domain.entities.Site;
import infrastructure.JpaUtil;
import infrastructure.data.interfaces.ISiteDAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;


public class SiteDAO extends GenericDAO<Site> implements ISiteDAO {
    public SiteDAO() {
        super(Site.class);
    }

    public Site getRSSFeedSite(String url){

        String sql = new String("select c from " + Site.class.getName() + " c  where url =:arg1");
        EntityManager entityManager = JpaUtil.getEntityManager();
        Query query = entityManager.createQuery(sql);
        query.setParameter("arg1", url);

        Site rssSite = null;
        try {
            rssSite = (Site)query.getResultList().get(0);//.getSingleResult();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }

        return rssSite;
    }
}
