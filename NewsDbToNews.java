package common.mapping;




import javafx.model.News;

import java.util.List;

public class NewsDbToNews implements GenericMapper<domain.entities.News, News> {

    private UserDbToUser userDbToUser;
    private SiteDbToSite siteDbToSite;
    private CategoryDbToCategory categoryDbToCategory;

    public NewsDbToNews() {
        this.userDbToUser = new UserDbToUser();
        this.siteDbToSite = new SiteDbToSite();
        this.categoryDbToCategory = new CategoryDbToCategory();
    }

    @Override
    public News map(domain.entities.News source) {

        return source!=null ? new News(source.getId(), source.getTitle(),
                source.getLink(),
                source.getDataAdded(),
                userDbToUser.map(source.getUser()),
                source.getSite() != null ? siteDbToSite.map(source.getSite()) : null,
                categoryDbToCategory.map(source.getCategory())) : null;
    }
}
