package services;

import common.DatabaseException;
import infrastructure.data.dao.UnitOfWork;
import javafx.AppNavigation;
import javafx.model.Right;
import jfx.messagebox.MessageBox;

import java.util.ArrayList;
import java.util.List;


public class RightService {
    private UnitOfWork unitOfWork;

    public RightService() {
        unitOfWork = new UnitOfWork();
    }

    public List<Right> get() {
        List<domain.entities.Right> rightsDb = null;
        try {
            rightsDb = unitOfWork.getRightDAO().get();
        } catch (DatabaseException e) {

            MessageBox.show(AppNavigation.getStage(),
                    "Database error",
                    "Error",
                    MessageBox.ICON_ERROR);
        }

        return map(rightsDb);
    }

    private List<Right> map(List<domain.entities.Right> rightsDb) {
        List<Right> rights = new ArrayList<>();
        for(domain.entities.Right rightDb : rightsDb){
            Right right = new Right(rightDb.getId(), rightDb.getName());
            rights.add(right);
        }
        return rights;
    }

    public Right getByName(String name){
        domain.entities.Right rightDb = unitOfWork.getRightDAO().getByName(name);
        if(rightDb != null){
            return new Right(rightDb.getId(), rightDb.getName());
        }
        return null;
    }
}
