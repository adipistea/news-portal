package services;

import common.DatabaseException;
import infrastructure.data.dao.UnitOfWork;
import javafx.AppNavigation;
import javafx.model.Category;
import jfx.messagebox.MessageBox;

import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class CategoryService {

    private UnitOfWork unitOfWork;

    public CategoryService() {
        this.unitOfWork = new UnitOfWork();
    }

    public List<Category> get(){
        List<domain.entities.Category> categoriesDb = null;
        try {
            categoriesDb = unitOfWork.getCategoryDAO().get();
        } catch (DatabaseException e) {
            MessageBox.show(AppNavigation.getStage(),
                    "Database error",
                    "Error",
                    MessageBox.ICON_ERROR);
        }

        return  map(categoriesDb);
    }

    public void add(Category category){
        domain.entities.Category categoryDb = new domain.entities.Category(
                category.getName(),
                category.getDescription(),
                null);
        unitOfWork.getCategoryDAO().create(categoryDb);
    }


    public void delete(long id) {
        unitOfWork.getCategoryDAO().remove(id);
    }

    private List<Category> map(List<domain.entities.Category> categoriesDb) {

        List<Category> categories = new ArrayList<>();

        for(domain.entities.Category category : categoriesDb){
            categories.add(new Category(category.getId(), category.getName(), category.getDescription()));
        }

        return categories;
    }
}
