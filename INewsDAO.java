package infrastructure.data.interfaces;

import common.DatabaseException;
import domain.entities.News;

import java.util.List;


public interface INewsDAO extends IGenericDAO<News> {
    List<News> getForUser(Long id) throws DatabaseException;
}
