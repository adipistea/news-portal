package domain.entities;

import javax.persistence.*;


@Entity
@Table(name = "Sites")
public class Site {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private Long id;

    @Column(name="Url")
    private String url;

    @Column(name="RssFeedUrl")
    private String rssFeedUrl;


    public Site(){}

    public Site(Long id){this.id = id;}

    public Site(String url, String rssFeedUrl) {
        this.url = url;
        this.rssFeedUrl = rssFeedUrl;
    }


    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getRssFeedUrl() {
        return rssFeedUrl;
    }
}
