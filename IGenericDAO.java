package infrastructure.data.interfaces;

import common.DatabaseException;

import java.util.List;

public interface IGenericDAO<T>{
    List<T> get() throws DatabaseException;

    T find(Long id);

    T create(T entity);

    T update(T entity);

    void remove(Long id);
}
